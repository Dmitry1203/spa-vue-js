<?php

namespace App\Http\Controllers\Factory;

class Factory
{
    public static function create($type, $val, $param = null)
	{
		
		// Документация https://www.php.net/manual/ru/language.oop5.basic.php
		// Если с директивой new используется строка (string), содержащая имя класса, то будет создан новый экземпляр этого класса. 
		// Если имя находится в пространстве имен, то оно должно быть задано полностью.
		
		$class = "App\Http\Controllers\Factory\\".$type;
        if (class_exists($class)) {
            return new $class($val, $param);
        } else {
            throw new \Exception("Class not found");
        }		
    }
}

?>