<?php

namespace App\Http\Controllers\Factory;

// класс для обработки e-mail полей, дополнительно проверяет формат e-mail
class FieldMail extends Input
{
	function __construct($val, $param){
		parent::__construct($val, $param);
		if (!empty($val) && !preg_match('/^(([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})@([-0-9A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $val)){
			$this->input['error'] = '&#10149; Укажите правильный формат e-mail';
			$this->isCorrect = false;
		}		
	}
}

?>