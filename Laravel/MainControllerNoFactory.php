<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MainController extends Controller
{

	// главная страница //
    public function index()	
    {
		$title = 'Главная страница';
		return view("main", compact('title'));
    }

	// показываем сообщения
    public function messages()	
    {

		$messages = DB::select("SELECT *, DATE_FORMAT(created_at,'%d.%m.%Y') as __created_at FROM messages ORDER BY id DESC");
		$title = 'Messages';
		return view("messages", compact('title', 'messages'));
		
    }

	// сохраняем сообщения
    public function createMessage()	
    {		
		
		/*
		
		Это классическая валидация Laravel, при работе с внешним SPA работать конечно не будет
		
		$rules = [
			'person_name' => 'required',
			'person_mail' => 'required|email',
			'person_message' => 'required',
        ];
        $errorMessages = [
            'person_name.required' => 'Укажите Ваше имя',
			'person_mail.required' => 'Укажите Ваш e-mail',
			'person_mail.email' => 'Некорректный e-mail',
			'person_message.unique' => 'Укажите Ваше сообщение',
        ];	

		$validator = Validator::make($request->all(), $rules, $errorMessages)->validate();	
		*/
		
		/////////////////////////////////////////////////////////////////////////////////////////////////
		//
		// Одна из реализации валидации, скорее тестовая, будет предоставлен еще один вариант, с фабрикой.
		// Для каждого поля (имя, e-mail, сообщение) создаем массив с полем value, куда просто запишем значение с проверкой на пробелы,
		// и с полем errorMsg, куда будет записано сообщение об ошибке, при наличии таковой.
		// Сообщение об ошибке выдаем в случае пустого значение value, и также, в случае некорректного e-mail.
		//
		// Если все поля errorMsg пусты, то считаем, что ошибки нет, и пишем запись в БД, используя подготовленный запрос,
		// и возвращаем ответ в формате JSON.
		// 
		// В данном примере не реализуется классическое API, для простоты используем ВИД response.blade.php, который
		// выдает необходимый заголовок и ответ для SPA VUE.
		// Еще как мне кажется особенность - в такой реализации пришлось отключить проверку CSRF для данного маршрута,
		// потому что SPA расположен на другом домене и соответственно об этом не знает.
		//
		/////////////////////////////////////////////////////////////////////////////////////////////////	
		
		// Имя пользователя
		$person['value'] = trim($_POST['personName']);
		$person['errorMsg'] = empty($person['value']) ? '&#10149; Укажите Ваше имя' : '';
		
		// E-mail
		$mail['value'] = trim($_POST['personMail']);		
		$mail['errorMsg'] = empty($mail['value']) ? '&#10149; Укажите Ваш e-mail' : '';				
		if (!empty($mail['value']) && !preg_match('/^(([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})@([-0-9A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $mail['value'])){
			$mail['errorMsg'] = '&#10149; Укажите правильный формат e-mail';
		}	
		
		// Сообщение пользователя
		$message['value'] = trim($_POST['personMsg']);
		$message['errorMsg'] = empty($message['value']) ? '&#10149; Укажите Ваше сообщение' : '';	
	
		$success = false;
	
		if (empty($person['errorMsg']) && empty($mail['errorMsg']) && empty($message['errorMsg'])){			
			$created = Date('Y-m-d');			
			$query = DB::insert("INSERT INTO messages (created_at,person_name,person_mail,person_message) VALUES (?, ?, ?, ?)", [$created, $person['value'], $mail['value'], $message['value']]);
			
			// Все успешно
			$success = true;
		} 
		
		// $response мы отдадим обратно SPA VUE, только в ВИДЕ добавим заголовки
		// header('Access-Control-Allow-Origin: *');
		// header('Content-type: application/json');
		// Это можно было как вариант, также прямо в return добавить.
				
		$response = [
			'success' => $success,
			'personNameError' => $person['errorMsg'],
			'personMailError' => $mail['errorMsg'],
			'personMsgError' => $message['errorMsg'],
		];
		
		return view("response", compact('response'));
		
    }
	
}
