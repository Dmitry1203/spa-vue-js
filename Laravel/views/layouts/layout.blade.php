<!DOCTYPE html>
<html lang="ru">
    <head>        
        <title>@isset($title){{ $title }}@endisset</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=$_ENV['APP_URL']?>public/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=$_ENV['APP_URL']?>public/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=$_ENV['APP_URL']?>public/favicon/favicon-16x16.png">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700;900&display=swap" rel="stylesheet"> 
		<link rel="stylesheet" type="text/css" href="<?=$_ENV['APP_URL']?>public/css/styles.css?<?=sha1(microtime(1))?>" />  
    </head>
    <body>	
	@yield('page')
	<script
	src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous">
	</script>
    </body>
</html>
