@extends('layouts.layout')
@section('page')
	<div class="container">	
	
		<h3 class="p-4 text-center">Получение данных из SPA VUE JS</h3>	
	
		<table class="table table-striped">
		<tr>		
		<th style="width:50px;">№</th>
		<th style="width:120px;">Дата</th>
		<th style="width:230px;">Имя пользователя</th>
		<th style="width:230px;">E-mail</th>
		<th>Сообщение</th>
		</tr>		
			@php
				$counter = 1;
			@endphp			
			@foreach ($messages as $message)
				<tr>				
				<td>{{ $counter }}</td>
				<td>{{ $message->__created_at }}</td>
				<td>{{ $message->person_name }}</td>
				<td>{{ $message->person_mail }}</td>
				<td>{{ $message->person_message }}</td>
				</tr>
				@php
					$counter++
				@endphp
			@endforeach	
		</table>
	</div>	
@endSection