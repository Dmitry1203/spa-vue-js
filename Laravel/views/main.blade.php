@extends('layouts.layout')
@section('page')
<section id="content">

	<div id="start-section">
		<div class="container vertical">
		<div class="text-center">
		<img src = "https://knigomania.ru/public/images/knigomania.png" class="img-fluid" alt="knigomania.ru">	
		</div>
		<p class="pt-4 f-size-24 text-justify">Knigomania — большая библиотека книг на русском языке. Представлены произведения всех жанров: детективы, фантастика, фэнтези, любовные романы, книги по бизнесу, психологии, для детей, современная и классическая литература.</p>
		</div>
	</div>
	
</section>
@endSection