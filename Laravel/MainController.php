<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

// импортируем для фабрики
use App\Http\Controllers\Factory\Factory;

// импортируем класс иконки в шаблоне
use App\Http\Controllers\Classes\ILink;

// импортируем для писем
use App\Mail\SendMail;

class MainController extends Controller
{

	// главная страница //
    public function index()	
    {
		$title = 'Главная страница';
		return view("main", compact('title'));
    }

	// показываем сообщения
    public function messages()	
    {
		// класс, который формирует массив иконок для использования на странице
		// компонент работает в связке css+js, при наведении как на иконку, так и на текст, двигает иконку
		// может быть несколько, как правило лежат во flex-контейнере

		$icons[] = ILink::create($_ENV['APP_URL'], "", "Главная страница");

		$messages = DB::select("SELECT *, DATE_FORMAT(created_at,'%d.%m.%Y') as __created_at FROM messages ORDER BY id DESC");
		$title = 'Messages';
		return view("messages", compact('title', 'messages', 'icons'));		
    }

	// сохраняем сообщения
    public function createMessage()	
    {		
		
		/*		
		Это классическая валидация Laravel, при работе с внешним SPA работать конечно не будет
		
		$rules = [
			'person_name' => 'required',
			'person_mail' => 'required|email',
			'person_message' => 'required',
        ];
        $errorMessages = [
            'person_name.required' => 'Укажите Ваше имя',
			'person_mail.required' => 'Укажите Ваш e-mail',
			'person_mail.email' => 'Некорректный e-mail',
			'person_message.unique' => 'Укажите Ваше сообщение',
        ];	

		$validator = Validator::make($request->all(), $rules, $errorMessages)->validate();	
		*/
				
		// Здесь для валидации параметров применяем класс Factory, который находится в App\Http\Controllers\Factory
		// Для каждого типа полей ввода на основе абстракного класса Input соответственно созданы классы FieldText, FieldMail и FieldTextarea
		// у которых собственная валидация.
		// Фабрика создает экземпляры классов по имени типа.		
		// Здесь для первого поля (Имя пользователя) передаем дополнительным параметром максимальное количество символов - 32.
		
		$personName = Factory::create('FieldText', $_POST['personName'], 32);
		$personMail = Factory::create('FieldMail', $_POST['personMail']);
		$personMessage = Factory::create('FieldTextarea', $_POST['personMsg']);
		
		// это результат действия, пока не произошли все проверки, по умолчанию false
		$success = false;
		
		if ($personName->isCorrect() && $personMail->isCorrect() && $personMessage->isCorrect()){
			// Все успешно	
			$created = Date('Y-m-d');

			$query = DB::insert("INSERT INTO messages (created_at,person_name,person_mail,person_message) VALUES (?, ?, ?, ?)", [$created, $personName->getValue(), $personMail->getValue(), $personMessage->getValue()]);

			$success = true;
			
			// отправим уведомление отправителю		

			$body = "<p><b>Здравствуйте {$personName->getValue()}, Ваше сообщение успешно отправлено.</b></p>";
			$body .= "<p style=\"margin:0 0 0 30px;padding:10px;border-left:2px solid #dadada\"><i>{$personMessage->getValue()}</i></p>";
			$body .= "<hr><p style=\"text-align:right;font-size:12px;padding:10px\">Были рады сотрудничать и т.д.</p>";		
			Mail::to($personMail->getValue())->send(new SendMail($body));
		
		} 

		$response = [
			'success' => $success,
			'personNameError' => $personName->getError(),
			'personMailError' => $personMail->getError(),
			'personMsgError' => $personMessage->getError(),
		];
				
		// view возвращает JSON
		
		return view("response", compact('response'));

    }
	
}
