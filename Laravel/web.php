<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;


//здесь вариант для версии 8, можно было бы в виде строки 'MainController@index'

Route::get('/', [MainController::class, 'index'])->name('main');

// Сообщения

Route::get('/messages', [MainController::class, 'messages'])->name('messages');

// Здесь сохраняем сообщения

Route::post('/create', [MainController::class, 'createMessage'])->name('createMessage');
	
// несуществующие адреса сюда, используется именованный маршрут main на главную
// если нет, то стандартная страница 404

Route::fallback(function () {
    //return redirect()->route('main');	
	abort (404);
});
